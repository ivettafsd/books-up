import sprite from '../img/sprite.svg';

export function renderUserInfo(userData) {
  return `<span class="box">
  <svg class="icon" width="37" height="37">
      <use href="${sprite}#icon-user"></use>
    </svg>
    ${userData.username}</span>`;
}
