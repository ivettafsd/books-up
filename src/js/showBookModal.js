import { fetchInfoBook } from './apiService';
import { renderInfoBook } from './renderInfoBook';
import { modal } from './modal';
import { renderShoppingBooks } from '../shopping-list';

export const showBookModal = async bookId => {
  const infoBook = await fetchInfoBook(bookId);
  localStorage.setItem('openInfoBook', JSON.stringify(infoBook));
  const renderedInfoBook = renderInfoBook(infoBook);
  modal(renderedInfoBook);

  const btnAddToShoppingList = document.querySelector('.book-modal .btnAdd');
  const btnDeleteFromShoppingList = document.querySelector(
    '.book-modal .btnDelete'
  );
  const shoppingBooks = JSON.parse(localStorage.getItem('shopping-books'));
  const openBook = JSON.parse(localStorage.getItem('openInfoBook'));
  const showAdd = () => {
    btnDeleteFromShoppingList.classList.remove('active');
    btnAddToShoppingList.classList.add('active');
  };
  const showRemove = () => {
    btnAddToShoppingList.classList.remove('active');
    btnDeleteFromShoppingList.classList.add('active');
  };
  const isAddShoppingList = shoppingBooks.find(book => book._id === openBook._id);
  isAddShoppingList ? showRemove() : showAdd();

  btnAddToShoppingList.addEventListener('click', event => {
    const openBook = JSON.parse(localStorage.getItem('openInfoBook'));
    const shoppingBooks = JSON.parse(localStorage.getItem('shopping-books'));

    const newShoppingBooks = shoppingBooks ? [...shoppingBooks, openBook] : [openBook];
    localStorage.setItem('shopping-books', JSON.stringify(newShoppingBooks));
    showRemove();
  });
  btnDeleteFromShoppingList.addEventListener('click', event => {
    const openBook = JSON.parse(
      localStorage.getItem('openInfoBook')
    );
    const shoppingBooks = JSON.parse(localStorage.getItem('shopping-books'));
    const newShoppingBooks = shoppingBooks.filter(book => book._id !== openBook._id);
    localStorage.setItem('shopping-books', JSON.stringify(newShoppingBooks));
    showAdd();
    renderShoppingBooks();
  });
};
