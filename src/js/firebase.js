import { initializeApp } from 'firebase/app';
import { getDatabase, set, ref, update, get } from 'firebase/database';
import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
} from 'firebase/auth';
import { renderUserInfo } from './renderUserInfo';

const signUpBtn = document.querySelector('.btn-sign-up');
const signInBtn = document.querySelector('.btn-sign-in');
const signOutBtn = document.querySelector('.btn-sign-out');

const firebaseConfig = {
  apiKey: 'AIzaSyBKh7wxmcEviZ9sMxgyBzA4vJ2lQPezfrw',
  authDomain: 'books-up.firebaseapp.com',
  databaseURL: 'https://books-up-default-rtdb.firebaseio.com',
  projectId: 'books-up',
  storageBucket: 'books-up.appspot.com',
  messagingSenderId: '855876954732',
  appId: '1:855876954732:web:3f47942e3ed6c348e78649',
};

const app = initializeApp(firebaseConfig);
const dataBase = getDatabase(app);

const auth = getAuth();

const handleCloseMenu = () => {
  const modalElem = document.querySelector('.modal[data-modal="auth"]');
  modalElem.classList.remove('active');
  const header = document.querySelector('.common-header .container');
  header.classList.remove('active');
  const overlay = document.querySelector('.js-overlay-modal');
  overlay && overlay.classList.remove('active');
};

const handleUserSignIn = async userSignedIn => {
  const user = userSignedIn.user;
  update(ref(dataBase, 'users/' + user.uid), {
    last_login: new Date(),
  });
  const userData = await get(ref(dataBase, 'users/' + user.uid));
  if (userData.exists()) {
    renderUserInfo(userData.val());
    const userBar = document.querySelector('.user-bar');
    if (userBar) {
      localStorage.setItem('currentUser', JSON.stringify(userData.val()));
    }
  } else {
    console.log('No data available');
  }
  handleCloseMenu();
  document.body.classList.add('authorized');
  document.body.classList.remove('auth');
};

const handleSignUp = async e => {
  e.preventDefault();
  const username = document.getElementById('up-username').value;
  const email = document.getElementById('up-email').value;
  const password = document.getElementById('up-password').value;
  try {
    const userCredential = await createUserWithEmailAndPassword(
      auth,
      email,
      password
    );
    if (userCredential) {
      const user = userCredential.user;
      set(ref(dataBase, 'users/' + user.uid), {
        username,
        email,
      });
      alert('user created');
      const userSignedIn = await signInWithEmailAndPassword(
        auth,
        email,
        password
      );
      userSignedIn && (await handleUserSignIn(userSignedIn));
    }
  } catch (error) {
    const errorCode = error.code;
    const errorMessage = error.message;
    alert(errorMessage);
  }
};
signUpBtn.addEventListener('click', e => handleSignUp(e));

const handleSignIn = async e => {
  e.preventDefault();
  const email = document.getElementById('in-email').value;
  const password = document.getElementById('in-password').value;
  const userSignedIn = await signInWithEmailAndPassword(auth, email, password);
  userSignedIn && (await handleUserSignIn(userSignedIn));
};
signInBtn.addEventListener('click', e => handleSignIn(e));

const handleSignOut = async e => {
  try {
    await signOut(auth);
    handleCloseMenu();
    document.body.classList.remove('authorized');
    const menuElem = document.querySelector('.modal[data-modal="menu"]');
    menuElem.classList.remove('active');
    localStorage.removeItem('currentUser');
  } catch (error) {
    const errorCode = error.code;
    const errorMessage = error.message;
    alert(errorMessage);
  }
};
signOutBtn.addEventListener('click', e => handleSignOut(e));
