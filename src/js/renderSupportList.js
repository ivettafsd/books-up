export function renderSupportList(supports) {
  return supports
    .map(
      (support, idx) => `<li class="support-item swiper-slide">
      <span class="counter">${idx + 1 < 10 ? `0${idx + 1}` : idx + 1}</span>
   <a href="${support.url}" target="_blank">
           <picture>
        <source srcset="${support.img} 1x, ${support.img2} 2x"  />
        <img class="img" src="${support.img}" alt="${support.title}" />
      </picture>
          </a>
   
 </li>`
    )
    .join('');
}
