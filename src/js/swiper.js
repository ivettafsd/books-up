import Swiper from 'swiper/bundle';

import 'swiper/css/bundle';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';

export const buildSwiperSliderSupport = sliderElm => {
  const sliderIdentifier = sliderElm.dataset.id;
  const swiper = new Swiper(`[data-id="${sliderIdentifier}"]`, {
    spaceBetween: 20,
    direction: 'vertical',
    slidesPerView: 'auto',
    loop: true,
    navigation: {
      nextEl: `.swiper-button-next-${sliderIdentifier}`,
      prevEl: `.swiper-button-prev-${sliderIdentifier}`,
    },
  });
  return swiper;
};

