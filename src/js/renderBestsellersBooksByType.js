export function renderBestsellersBooksByType(data, typeBooks) {
  return `
<h3 class="title">${typeBooks.substring(
    0,
    typeBooks.lastIndexOf(' ')
  )}<span class="accent"> ${typeBooks.split(' ').pop()}</span></h3>

     ${
       data.length > 0
         ? `<ul class="bestsellers-books-by-type__list">
     ${data
       .map(
         book => `<li class="bestseller-book">
 
          <div class="imgBox" data-id="${book._id}" data-type="${
           typeBooks.list_name
         }">
    <img
            class="img"
            src="${book.book_image ? book.book_image : ``}"
            alt="${book.title}"
            loading="lazy"
          />
          <p class="imgBtn">Quick view</p>
</div>
          <p class="bookTitle">
            ${
              book.title.length > 14
                ? book.title.slice(0, 14) + '...'
                : book.title
            }
    			</p>
          <p class="bookAuthor">
            ${
              book.author.length > 28
                ? book.author.slice(0, 28) + '...'
                : book.author
            } 
          </p>
   
    </li>`
       )
       .join('')}
     </ul>`
         : `<p>Not found</p>`
     }
    `;
}
