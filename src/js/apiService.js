import axios from 'axios';

axios.defaults.baseURL = 'https://books-backend.p.goit.global';

export const fetchBestsellersBooks = async () => {
  try {
    const { data } = await axios.get(`/books/top-books`);
    return data;
  } catch (error) {
    console.log(error);
    return [];
  }
};

export const fetchCategories = async () => {
  try {
    const { data } = await axios.get(`/books/category-list`);
    return data;
  } catch (error) {
    console.log(error);
    return [];
  }
};

export const fetchInfoBook = async bookId => {
  try {
    const { data } = await axios.get(`/books/${bookId}`);
    return data;
  } catch (error) {
    console.log(error);
  }
};

export const fetchTypeBooksMore = async type => {
  try {
    const { data } = await axios.get(`/books/category?category=${type}`);
    return data;
  } catch (error) {
    return [];
  }
};
