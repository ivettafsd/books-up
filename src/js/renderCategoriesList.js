export function renderCategoriesList(categories) {
  return `<li class="category" data-id="all-categories">
  All categories
 </li>
    ${categories
      .map(
        category => `<li class="category" data-id="${category.list_name}">
  ${category.list_name}
 </li>`
      )
      .join('')}
    `;
}
