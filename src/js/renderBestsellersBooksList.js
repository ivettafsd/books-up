import { renderBestsellersBook } from './renderBestsellersBook';

export function renderBestsellersBooksList(data) {
  return data.length > 0
    ? data
        .map((typeBooks, idx) => {
          return `
    <li class="bestsellers-typeBooks">
       <h3 class="title">${typeBooks.list_name}</h3>
  
          <ul class="booksList"> 
              ${typeBooks.books
                .map(book => renderBestsellersBook(book, typeBooks))
                .join('')}
          </ul>
  
<div class="btnBox">
 <button class="btn" type="button" data-id="${
   typeBooks.list_name
 }">See more</button></div>
    </li>`;
        })
        .join('')
    : `<p>Not found</p>`;
}
