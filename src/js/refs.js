export const refs = {
  homeBestsellers: document.querySelector('.home-bestsellers'),
  homeBestsellersByType: document.querySelector('.home-bestsellers-by-type'),
  categoriesContainer: document.querySelector('.categories__list'),
  supportContainer: document.querySelector('.support__list'),
  bestsellersBooksContainer: document.querySelector('.bestsellers-books__list'),
  sliderSupport: document.querySelector('.swiperSupport'),
  shoppingBooksContainer: document.querySelector('.shopping-books'),
  paginationContainer: document.getElementById('pagination'),
  signUpBtn: document.querySelector('.auth-up')
};
