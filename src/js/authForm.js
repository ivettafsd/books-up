document.addEventListener('DOMContentLoaded', function () {
  var modalButton = document.querySelector('.auth-up.js-open-modal'),
    overlay = document.querySelector('.js-overlay-modal');
  // closeButtons = document.querySelectorAll('.menu-close'),
  // header = document.querySelector('.common-header .container');
  modalButton.addEventListener('click', function (e) {
    e.preventDefault();
    const menuElem = document.querySelector('.modal[data-modal="menu"]');
    var header = document.querySelector('.common-header .container');
    menuElem.classList.remove('active');
    header.classList.remove('active');
    const authModal = document.querySelector('.auth-modal');
    authModal.classList.add('active');
    overlay && overlay.classList.remove('active');
    document.body.classList.remove('stop-scroll');

    document.body.classList.add('auth');
    var modalId = this.getAttribute('data-modal'),
      modalElem = document.querySelector(
        '.modal[data-modal="' + modalId + '"]'
      );
    modalElem.classList.add('active');

    overlay && overlay.classList.add('active');
  });
  let radios = document.querySelectorAll('.form .auth-radio');

  for (let i = 0, max = radios.length; i < max; i++) {
    radios[i].addEventListener('click', function (e) {
      const forms = ['sign-up', 'sign-in'];
      const currentForm = document.querySelector(`.${e.target.value}`);
      const oldForm = document.querySelector(
        `.${forms.find(item => item !== e.target.value)}`
      );
      oldForm.classList.remove('active');
      currentForm.classList.add('active');
    });
  }
  //closeButtons.forEach(function (item) {
  //  item.addEventListener('click', function (e) {
  //    const modalElem = document.querySelector('.modal[data-modal="menu"]');
  //   modalElem.classList.remove('active');
  //   header.classList.remove('active');
  //    overlay && overlay.classList.remove('active');
  //  });
  // });
  document.body.addEventListener(
    'keyup',
    function (e) {
      var key = e.keyCode;

      if (key == 27) {
        document.querySelector('.modal.active').classList.remove('active');
        document.querySelector('.overlay').classList.remove('active');
        document.body.classList.remove('stop-scroll');
      }
    },
    false
  );

  overlay?.addEventListener('click', function () {
    document.querySelector('.modal.active').classList.remove('active');
    this.classList.remove('active');
    document.body.classList.remove('stop-scroll');
  });
});
