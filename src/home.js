import { refs } from './js/refs';
import { renderBestsellersBooksList } from './js/renderBestsellersBooksList';
import { renderCategoriesList } from './js/renderCategoriesList';
import { buildSwiperSliderSupport } from './js/swiper';
import { showBookModal } from './js/showBookModal';
import { fetchBestsellersBooks, fetchCategories } from './js/apiService';
import { showTypeBooksMore } from './js/showTypeBooksMore';
import { renderSupport } from './js/supportList';
import './js/swiper';
import './js/apiService';
import './js/menu';
import './js/theme';
import './js/firebase';

let currentRenderWidth = 375;

addEventListener('resize', event => {
  if (
    (window.innerWidth > 767 && currentRenderWidth < 768) ||
    (window.innerWidth > 1439 && currentRenderWidth < 1440) ||
    (window.innerWidth < 1440 && currentRenderWidth > 1439) ||
    (window.innerWidth < 768 && currentRenderWidth > 767)
  ) {
    location.reload();
  }
});

const renderSliderSupport = async () => {
  buildSwiperSliderSupport(refs.sliderSupport);
};
renderSliderSupport();

const renderBestsellersBooks = async () => {
  currentRenderWidth = window.innerWidth;
  let amountRenderedBooks = 1;
  if (currentRenderWidth < 768) {
    amountRenderedBooks = 1;
  } else if (currentRenderWidth > 767 && currentRenderWidth < 1440) {
    amountRenderedBooks = 3;
  } else {
    amountRenderedBooks = 5;
  }
  let bestsellersBooks = await fetchBestsellersBooks();
  bestsellersBooks = bestsellersBooks.map(type => {
    return { ...type, books: type.books.slice(0, amountRenderedBooks) };
  });

  refs.homeBestsellersByType.classList.remove('active');
  refs.homeBestsellers.classList.add('active');
  refs.bestsellersBooksContainer.innerHTML = await renderBestsellersBooksList(
    bestsellersBooks
  );

  const booksBestsellers = document.querySelectorAll('.booksList');
  booksBestsellers.forEach(book => {
    book.addEventListener('click', event => {
      if (event.target.parentNode.nodeName === 'DIV')
        showBookModal(event.target.parentNode.dataset.id);
      if (event.target.nodeName === 'DIV') {
        showBookModal(event.target.dataset.id);
      }
      document.body.classList.add('stop-scroll');
    });
  });

  const allBtnsSeeMore = document.querySelectorAll(
    '.bestsellers-typeBooks .btn'
  );
  allBtnsSeeMore.forEach(btn => {
    btn.addEventListener('click', event => {
      showTypeBooksMore(event.target.dataset.id);
    });
  });
};
renderBestsellersBooks();

const renderCategories = async () => {
  const categories = await fetchCategories();
  refs.categoriesContainer.innerHTML = await renderCategoriesList(categories);

  const allBtnsCategory = document.querySelectorAll('.category');
  allBtnsCategory.forEach(btnCategory => {
    btnCategory.addEventListener('click', event => {
      const oldActiveCategory = document.querySelector('.category.active');
      if (oldActiveCategory) {
        oldActiveCategory.classList.remove('active');
      }
      event.target.classList.add('active');
      if (event.target.dataset.id === 'all-categories') {
        renderBestsellersBooks();
      } else {
        showTypeBooksMore(event.target.dataset.id);
      }
    });
  });
};
renderCategories();

renderSupport();

const currentShoppingList = JSON.parse(localStorage.getItem('shopping-books'));
if (!currentShoppingList)
  localStorage.setItem('shopping-books', JSON.stringify([]));

const currentUser = JSON.parse(localStorage.getItem('currentUser'));
if (currentUser) {
  document.body.classList.add('authorized');
} else {
  document.body.classList.remove('authorized');
}
