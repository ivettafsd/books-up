import { renderUserInfo } from './renderUserInfo';
import './authForm';

!(function (e) {
  'function' != typeof e.matches &&
    (e.matches =
      e.msMatchesSelector ||
      e.mozMatchesSelector ||
      e.webkitMatchesSelector ||
      function (e) {
        for (
          var t = this,
            o = (t.document || t.ownerDocument).querySelectorAll(e),
            n = 0;
          o[n] && o[n] !== t;

        )
          ++n;
        return Boolean(o[n]);
      }),
    'function' != typeof e.closest &&
      (e.closest = function (e) {
        for (var t = this; t && 1 === t.nodeType; ) {
          if (t.matches(e)) return t;
          t = t.parentNode;
        }
        return null;
      });
})(window.Element.prototype);

async function handleOpenMenu(e) {
  e.preventDefault();
  const modalId = this.getAttribute('data-modal');
  const modalElem = document.querySelector(
    '.modal[data-modal="' + modalId + '"]'
  );
  const header = document.querySelector('.common-header .container');
  const overlay = document.querySelector('.js-overlay-modal');

  header.classList.add('active');
  modalElem.classList.add('active');
  const currentUser = JSON.parse(localStorage.getItem('currentUser'));
  if (currentUser) {
    const userBar = document.querySelector('.user-bar');
    userBar.innerHTML = await renderUserInfo(currentUser);
    overlay && overlay.classList.add('active');
    document.body.classList.add('stop-scroll');
  } else {
    // document.body.classList.remove('authorized');
  }
}

document.addEventListener('DOMContentLoaded', function () {
  const modalButtons = document.querySelectorAll('.js-open-modal');
  const overlay = document.querySelector('.js-overlay-modal');
  const closeButtons = document.querySelectorAll('.menu-close');
  const header = document.querySelector('.common-header .container');

  modalButtons.forEach(function (item) {
    item.addEventListener('click', handleOpenMenu);
  });
  closeButtons.forEach(function (item) {
    item.addEventListener('click', function (e) {
      const modalElem = document.querySelector('.modal[data-modal="menu"]');
      modalElem.classList.remove('active');
      header.classList.remove('active');
      overlay && overlay.classList.remove('active');
      document.body.classList.remove('stop-scroll');
    });
  });

  document.body.addEventListener(
    'keyup',
    function (e) {
      var key = e.keyCode;

      if (key == 27) {
        document.querySelector('.modal.active').classList.remove('active');
        document.querySelector('.overlay').classList.remove('active');
        document.body.classList.remove('stop-scroll');
        header.classList.remove('active');
      }
    },
    false
  );

  overlay?.addEventListener('click', function () {
    document.querySelector('.modal.active').classList.remove('active');
    this.classList.remove('active');
    header.classList.remove('active');
    document.body.classList.remove('stop-scroll');
  });
});
