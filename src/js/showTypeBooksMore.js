import AirDatepicker from 'air-datepicker';
import localeEn from 'air-datepicker/locale/en';
import 'air-datepicker/air-datepicker.css';
import { fetchTypeBooksMore } from './apiService';
import { showBookModal } from './showBookModal';
import { renderBestsellersBooksByType } from './renderBestsellersBooksByType';
import { refs } from './refs';

export const showTypeBooksMore = async type => {
  const typeBooksMore = await fetchTypeBooksMore(type);

  refs.homeBestsellers.classList.remove('active');
  refs.homeBestsellersByType.classList.add('active');
  refs.homeBestsellersByType.innerHTML = renderBestsellersBooksByType(
    typeBooksMore,
    type
  );
  const books = document.querySelectorAll('.bestsellers-books-by-type__list');
  books.forEach(book => {
    book.addEventListener('click', event => {
      if (event.target.parentNode.nodeName === 'LI')
        showBookModal(
          event.target.parentNode.dataset.id,
        );
    });
  });
};
