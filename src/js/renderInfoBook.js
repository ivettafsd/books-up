import sprite from '../img/sprite.svg';

export function renderInfoBook(infoBook) {
  return `
  <div class="book-modal">
     <svg class="close" width="24" height="24">
             <use href="${sprite}#icon-close"></use>
      </svg>
    <div class="content">
     <div class="book">
      <img
        class="cover"
        src="${infoBook.book_image ? infoBook.book_image : ``}"
        alt="${infoBook.title}"
        loading="lazy"
       />
      <div class="text">
        <h4 class="title">${infoBook.title}</h4>
        <h4 class="author">${infoBook.author}</h4>
        <p class="description">${infoBook.description}</p>
        

        <div class="links">
          ${infoBook.buy_links
            .filter(
              link =>
                link.name === 'Amazon' ||
                link.name === 'Bookshop' ||
                link.name === 'Apple Books'
            )
            .map(link => {
              return `<a class="link" href=${link.url}>
              <img
              class="img ${link.name}"
              src="/${link.name}.png"
              alt="Shop logo"
  
            />
            </a>`;
            })
            .join('')}
        </div>
      </div>
     </div>
     <button class="btn btnAdd active" type="button">Add to shopping list</button>
     <div class="btnDelete">
         <button class="btn" type="button">Remove from shopping list</button>
     </div>

    </div>
  </div>
    `;
}
